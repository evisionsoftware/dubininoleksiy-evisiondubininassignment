﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using AccountsApp;

namespace AccountsApp.Tests.FakeClasses
{
    internal class AccountServiceFake : IAccountService
    {
        private readonly double GET_ACCOUNT_AMOUNT_DELAY = 5; // in seconds 

        private readonly Dictionary<int, double> _amounts = new Dictionary<int, double>();

        public AccountServiceFake(Dictionary<int, double> amounts)
        {
            _amounts = amounts;
        }

        public double GetAccountAmount(int accountId)
        {
            Thread.Sleep(TimeSpan.FromSeconds(GET_ACCOUNT_AMOUNT_DELAY)); // Simulate loooong running method
            
            if (!_amounts.ContainsKey(accountId))
            {
                throw new KeyNotFoundException("Account identifier not found");
            }

            return _amounts[accountId];
        }
    }
}
