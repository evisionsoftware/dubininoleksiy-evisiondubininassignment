﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Collections.Generic;

using AccountsApp;
using AccountsApp.Tests.FakeClasses;

namespace AccountsApp.Tests
{
    [TestClass]
    public class AccountInfoTest
    {
        private readonly double ZERO_AMOUNT = 0d;

        private IAccountService _accountService;

        [TestInitialize]
        public void SetupAccountService()
        {
            var amounts = new Dictionary<int, double>
            {
                {1, 1.18d},
                {2, 3.12d},
                {3, 5.32d},
                {4, 78.12d},
                {8, -98.57d},
                {10, ZERO_AMOUNT},
                {15, double.MinValue},
                {20, double.MaxValue},
                {57, 746.12d}
            };

            _accountService = new AccountServiceFake(amounts);
        }

        [TestMethod]
        public void Positive_Amount_Is_Correct()
        {
            // arrange
            int accountId = 4;
            double expectedAmount = 78.12d;

            var accountInfo = new AccountInfo(accountId, _accountService);

            // act
            accountInfo.RefreshAmount();

            // assert
            Assert.AreEqual(expectedAmount, accountInfo.Amount);
        }

        [TestMethod]
        public void Negative_Amount_Is_Correct()
        {
            // arrange
            int accountId = 8;
            double expectedAmount = -98.57d;

            var accountInfo = new AccountInfo(accountId, _accountService);

            // act
            accountInfo.RefreshAmount();

            // assert
            Assert.AreEqual(expectedAmount, accountInfo.Amount);
        }

        [TestMethod]
        public void Minimal_Amount_Is_Correct()
        {
            // arrange
            int accountId = 15;
            double expectedAmount = double.MinValue;

            AccountInfo accountInfo = new AccountInfo(accountId, _accountService);

            // act
            accountInfo.RefreshAmount();

            // assert
            Assert.AreEqual(expectedAmount, accountInfo.Amount);
        }

        [TestMethod]
        public void Maximal_Amount_Is_Correct()
        {
            // arrange
            int accountId = 20;
            double expectedAmount = double.MaxValue;

            var accountInfo = new AccountInfo(accountId, _accountService);

            // act
            accountInfo.RefreshAmount();

            // assert
            Assert.AreEqual(expectedAmount, accountInfo.Amount);
        }

        [TestMethod]
        public void Zero_Amount_Is_Correct()
        {
            // arrange
            int accountId = 10;
            double expectedAmount = ZERO_AMOUNT;

            AccountInfo accountInfo = new AccountInfo(accountId, _accountService);

            // act
            accountInfo.RefreshAmount();

            // assert
            Assert.AreEqual(expectedAmount, accountInfo.Amount);
        }

        [TestMethod]
        public void Amount_Is_Changed_After_Refresh()
        {
            // arrange
            int accountId = 4;

            AccountInfo accountInfo = new AccountInfo(accountId, _accountService);

            double initialAmount = accountInfo.Amount;

            // act
            accountInfo.RefreshAmount();

            // assert
            Assert.AreNotEqual(initialAmount, accountInfo.Amount);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void AccountId_Not_Exists()
        {
            // arrange
            int notExistingAccountId = 7;

            AccountInfo accountInfo = new AccountInfo(notExistingAccountId, _accountService);

            // act
            accountInfo.RefreshAmount();

            // assert
            // No assertation
        }
    }
}
